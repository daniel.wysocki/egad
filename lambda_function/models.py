from pydantic import BaseModel


class Response(BaseModel):
    statusCode: int
    body: str


class KafkaAlert(BaseModel):
    topics: list[str]
    message: str


class Contact(BaseModel):
    phone_number: str
    phone_method: str


class PhoneAlert(BaseModel):
    contacts: list[Contact]
    message: str
    twiml_url: str


class EmailAlert(BaseModel):
    recipients: list[str]
    subject: str
    body: str


class MattermostAlert(BaseModel):
    channel_label: str
    message: str


class Event(BaseModel):
    type: str
    tier: str
    contents: dict
