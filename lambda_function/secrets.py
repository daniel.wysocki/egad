import json

import boto3
from botocore.exceptions import ClientError


# Mapping of (str region_name, str secret_name) -> dict secrets
__secrets = {}

# Mapping of str region_name -> boto3 client
__boto3_clients = {}


def get_secrets(region_name, secret_name):
    if (region_name, secret_name) in __secrets:
        # Return cached secrets
        return __secrets[region_name, secret_name]

    if region_name in __secrets:
        # Use cached boto3 client
        client = __boto3_clients[region_name]
    else:
        # Create a Secrets Manager client
        session = boto3.session.Session()
        client = session.client(
            service_name='secretsmanager',
            region_name=region_name
        )

    try:
        # Try to get the secret
        get_secret_value_response = client.get_secret_value(
            SecretId=secret_name
        )
    except ClientError as e:
        # For a list of exceptions thrown, see
        # https://docs.aws.amazon.com/secretsmanager/latest/apireference/API_GetSecretValue.html
        raise e

    # Decrypt secret using the associated KMS key.
    secret_str = get_secret_value_response['SecretString']

    # Parse the secret from JSON to a dict and return
    return json.loads(secret_str)
