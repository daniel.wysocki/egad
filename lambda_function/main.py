import os
import json
import itertools

from aws_xray_sdk.core import patch_all

from codeguru_profiler_agent import with_lambda_profiler

from pydantic import ValidationError

from . import dispatchers
from . import models
from . import responses
from .status import is_provisioned_concurrency

# Apply AWS X-ray patch
patch_all()

print(f"ENV: {os.environ}")

alert_handlers = {
    "email": {
        "dispatcher": dispatchers.EmailDispatcher,
        "model": models.EmailAlert,
    },
    "kafka": {
        "dispatcher": dispatchers.KafkaDispatcher,
        "model": models.KafkaAlert,
    },
    "phone": {
        "dispatcher": dispatchers.PhoneDispatcher,
        "model": models.PhoneAlert,
    },
    "mattermost": {
        "dispatcher": dispatchers.MattermostDispatcher,
        "model": models.MattermostAlert,
    },
}


# Initialize handlers when using provisioned concurrency
if is_provisioned_concurrency:
    fast_endpoints = json.loads(os.environ.get("FAST_ENDPOINTS", "[]"))
    fast_tiers = json.loads(os.environ.get("FAST_TIERS", "[]"))
    for endpoint, tier in itertools.product(fast_endpoints, fast_tiers):
        handler = alert_handlers[endpoint]
        dispatcher = handler["dispatcher"](tier)


def lambda_handler_logic(event, context):
    print(f"{event=}")
    print(f"{context=}")

    try:
        event = models.Event.parse_obj(event)
    except ValidationError as err:
        print(str(err))
        return responses.custom(
            statusCode=422,
            body=str(err),
        )

    alert_type = event.type
    tier = event.tier
    raw_contents = event.contents

    try:
        handler = alert_handlers[alert_type]
    except KeyError:
        msg_prefix = f"{alert_type=} unsupported, must be one of: "
        msg = msg_prefix + ", ".join(alert_handlers.keys())

        return responses.custom(
            statusCode=422,
            body=msg,
        )

    dispatcher_cls = handler["dispatcher"]
    model = handler["model"]

    dispatcher = dispatcher_cls(tier)
    contents = model.parse_obj(raw_contents)

    return dispatcher.handle(contents, context)


@with_lambda_profiler()
def lambda_handler(event, context):
    response = lambda_handler_logic(event, context)

    return response.dict()
