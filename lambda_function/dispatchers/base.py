import abc
import traceback

from .. import responses


class Dispatcher(metaclass=abc.ABCMeta):
    def handle(self, contents, context) -> str:
        try:
            status, message = self._handle(contents, context)
        except Exception:
            traceback.print_exc()

            status = 500
            message = (
                f"Unhandled error, see log stream '{context.log_stream_name}'"
            )

        return responses.custom(
            statusCode=status,
            body=message,
        )

    @abc.abstractmethod
    def _handle(self, contents, context) -> tuple[int, str]:
        """
        Handle event and return HTTP status code and message.
        """
        ...
