from hashlib import sha1
import json
import os
import time

import requests

from aws_xray_sdk.core import xray_recorder

from .base import Dispatcher
from ..secrets import get_secrets


mattermost_endpoint = "https://chat.ligo.org/api/v4/posts"

_tier_to_headers_and_channel_map = {}


class MattermostDispatcher(Dispatcher):
    @xray_recorder.capture("## init_mattermost")
    def __init__(self, tier):
        if tier not in _tier_to_headers_and_channel_map:
            time_start = time.perf_counter()

            region_name = os.environ.get("SECRETS_REGION", "us-west-2")
            secret_name = f"EGAD_Credentials/{tier}"

            secrets = get_secrets(region_name, secret_name)

            mattermost_bot_key = secrets["MATTERMOST_BOT_KEY"]
            mattermost_channel_map = json.loads(
                secrets["MATTERMOST_CHANNEL_MAP"]
            )

            headers = {"Authorization": f"Bearer {mattermost_bot_key}"}

            _tier_to_headers_and_channel_map[tier] = (
                headers, mattermost_channel_map
            )

            time_elapsed = time.perf_counter() - time_start
            print(f"Set up Mattermost credentials in {time_elapsed} sec")

        else:
            print("Using existing Mattermost credentials")

        self.headers, self.channel_map = _tier_to_headers_and_channel_map[tier]

    @xray_recorder.capture("## handle_mattermost")
    def _handle(self, contents, context) -> tuple[int, str]:
        channel_label = contents.channel_label
        message = contents.message

        channel_id = self.channel_map[channel_label]

        message_id = sha1(
            f"{channel_id}:{message}".encode("utf-8")
        ).hexdigest()

        payload = {
            "channel_id": channel_id,
            "message": message,
        }
        print(f"sending Mattermost message {message_id} to {channel_id}")
        r = requests.post(
            mattermost_endpoint,
            headers=self.headers, json=payload,
        )

        if r.status_code == 200:
            return 200, "Success"

        print(f"failed to send Mattermost message {message_id}:\n"
              f"Status: {r.status_code}\nBody:\n{r.text}")

        return 422, f"Failed to send message {message_id}"
