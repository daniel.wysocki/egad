#!/bin/bash

# Fail if there are any errors (-e) and undefined variables (-u)
set -eu

# Ensure an EGAD version was specified.
if [[ -z "$EGAD_VERSION" ]]
then
    >&2 echo "Environment variable EGAD_VERSION must be set to the version to be published"
    exit 1
fi

# Ensure a Python version was specified
if [[ -z "$PYTHON_VERSION" ]]
then
    >&2 echo "Environment variable PYTHON_VERSION must be set to the Python version to use"
    exit 1
fi

# Ensure the S3 bucket was specified
if [[ -z "$EGAD_S3_BUCKET" ]]
then
    >&2 echo "Environment variable EGAD_S3_BUCKET must be set to the S3 bucket where the package is to be uploaded"
    exit 1
fi

# Give default value to EGAD_PUBLISH
EGAD_PUBLISH="${EGAD_PUBLISH:-false}"

# Construct the full path to the object in S3
VERSION_SUFFIX="${EGAD_VERSION}-${PYTHON_VERSION}"
ZIP_FILE="build/package-${VERSION_SUFFIX}.zip"
PACKAGE_S3_PATH="${EGAD_S3_BUCKET}/package-${VERSION_SUFFIX}.zip"

# Only upload files with versions like vX.Y.Z[rcN] if EGAD_PUBLISH=true, and don't
# overwrite an existing file with that name.
if [[ "$EGAD_VERSION" =~ v[0-9]+\.[0-9]+\.[0-9]+(rc[0-9]+)?$ ]]
then
    # Case-insensitive check of EGAD_PUBLISH != "true"
    if [ "${EGAD_PUBLISH,,}" != "true" ]
    then
        >&2 echo "You must set EGAD_PUBLISH=true if you want to publish a version matching vX.Y.Z[rcN]"
        exit 2
    fi

    # Fail if file already exists
    aws s3 ls "$PACKAGE_S3_PATH" && >&2 echo "Cannot overwrite already published version" && exit 3
fi

# Upload the file to S3
aws s3 cp "$ZIP_FILE" "$PACKAGE_S3_PATH"
