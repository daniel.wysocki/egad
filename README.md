# External GraceDB Alert Dispatcher (EGAD)

This is EGAD, a serverless microservice used to dispatch various types of
alerts on behalf of GraceDB.


## Releases

GitLab will automatically build and upload the distribution to
[AWS S3](https://aws.amazon.com/s3/) whenever a tag is made.  Tags should be of
the form `vX.Y.Z` for full releases, and `vX.Y.ZrcN` for release candidates.
The upload scripts will refuse to overwrite existing files matching these
versioning rules, to avoid data loss, but other names like `my-test-version`
can be.

If git.ligo.org is down, or you just want to test things without it, you can
run the build and upload scripts yourself.  You will need the
[AWS CLI v2](https://docs.aws.amazon.com/cli/latest/userguide/getting-started-install.html)
as well as the Python version currently used.  Then run:
```bash
# Authenticate with AWS using the wizard or environment variables
$ aws credentials
# or
export AWS_ACCESS_KEY_ID=...
export AWS_SECRET_ACCESS_KEY=...

# Define all necessary environment variables
export EGAD_VERSION=my-test-version
export PYTHON_VERSION=pythonX.Y
export EGAD_S3_BUCKET=s3://name-of-bucket

# Build the Zip archive
bin/build_zip.sh

# Upload to S3
bin/upload_s3.sh
```

As a safety measure, `upload_s3.sh` will refuse to upload protected versions
like `vX.Y.Z` unless you also `export EGAD_PUBLISH=true`.
